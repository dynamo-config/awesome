-- Custom library
local run_command = require('lib.shell').run_command
local helpers = require("vicious.helpers")

local nvidia_gpu = {}

function nvidia_gpu.async(_, warg, callback)
  warg = warg or '0'
  run_command(
    'nvidia-smi --query-gpu=memory.used --format=csv,noheader -i ' .. warg,
    true,
    function(stdout, _, _, _)
      local result = string.gsub(stdout, '%s+$', '')
      return callback({ result })
    end,
    function() return callback({ 0 }) end,
    true
  )
end

return helpers.setasyncall(nvidia_gpu)
