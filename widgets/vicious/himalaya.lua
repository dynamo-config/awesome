-- Custom library
local run_command = require('lib.shell').run_command
local helpers = require("vicious.helpers")

local himalaya = {}

function himalaya.async(_, warg, callback)
  warg = warg or ''
  run_command(
    "himalaya --output json search UNSEEN " .. warg .. " | jq '.[].id' | wc -l",
    true,
    function(stdout, _, _, _)
      local result = string.gsub(stdout, '%s+$', '')
      return callback({ result })
    end,
    function() return callback({ 0 }) end,
    true
  )
end

return helpers.setasyncall(himalaya)
